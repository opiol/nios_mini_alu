
#include "sys/alt_stdio.h"
#include "io.h"

#define ALU_BASE 	0x9000
#define ALU_A 		(0)
#define ALU_B 		(4)
#define ALU_OP	  	(8)
#define ALU_RES   	(12)
#define ALU_STAT	(16)

#define OP_AND		(1)
#define OP_OR		(2)
#define OP_ADD		(3)
#define OP_SUB		(4)
#define OP_NOT		(5)

char operations[][4] = { "NaN" ,"AND", "OR", "ADD", "SUB", "NOT"};

void show(void)
{
	alt_printf( operations[ IORD(ALU_BASE,ALU_OP) ] );
	alt_printf(": valA: 0x%x  valB: 0x%x  result: 0x%x  ",IORD(ALU_BASE,ALU_A),
			 	 	 	 	 	 	 	 	 	 	 	  IORD(ALU_BASE,ALU_B),
			 	 	 	 	 	 	 	 	 	 	 	  IORD(ALU_BASE,ALU_RES));
	alt_printf( "ovf:%x  udf:%x\n",IORD(ALU_BASE,ALU_STAT) & 0x1,
								  (IORD(ALU_BASE,ALU_STAT) & 0x2) >> 1 );
}

void alu(alt_u32 a, alt_u32 b, alt_u32 op)
{
	 IOWR(ALU_BASE, ALU_A, a);
	 IOWR(ALU_BASE, ALU_B, b);
	 IOWR(ALU_BASE, ALU_OP, op);
}

int main()
{ 
	alt_putstr("Hello from Nios II!\n");

	alu(127, 126, OP_SUB); show();
	alu(127, 256, OP_SUB); show();

	alu(2, 6, OP_ADD); show();
	IORD(ALU_BASE,ALU_RES);
	alu(0xfffffffe, 0x0ff, OP_ADD); show();

	alu(0xffffffff, 0xff00ff00,OP_AND); show();
	alu(0xffffffff, 0xff00ff00,OP_OR); show();
	alu(0, 0, OP_NOT); show();

	while (1);
	return 0;
}
