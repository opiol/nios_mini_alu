**************** 1st post tap         *****************

.lib tx_1tap0 
vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
.endl

.lib tx_1tap1 
vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
.endl

*.lib tx_1tap2 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
*.endl

*.lib tx_1tap3 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
*.endl

*.lib tx_1tap4 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
*.endl

.lib tx_1tap5 
vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
.endl

*.lib tx_1tap6 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
*.endl

*.lib tx_1tap7 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
*.endl

*.lib tx_1tap8 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
*.endl

.lib tx_1tap9 
vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
.endl

*.lib tx_1tap10 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
*.endl

*.lib tx_1tap11 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
*.endl

*.lib tx_1tap12 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
*.endl

.lib tx_1tap13 
vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
.endl

*.lib tx_1tap14 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
*.endl

*.lib tx_1tap15 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       0
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
*.endl

.lib tx_1tap16 
vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
.endl

.lib tx_1tap17 
vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
.endl

.lib tx_1tap18 
vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
.endl

.lib tx_1tap19 
vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
.endl

.lib tx_1tap20 
vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
.endl

.lib tx_1tap21 
vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
.endl

*.lib tx_1tap22 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
*.endl

*.lib tx_1tap23 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       0
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
*.endl

*.lib tx_1tap24 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
*.endl

*.lib tx_1tap25 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
*.endl

*.lib tx_1tap26 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
*.endl

*.lib tx_1tap27 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       0
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
*.endl

*.lib tx_1tap28 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
*.endl

*.lib tx_1tap29 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       0
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
*.endl

*.lib tx_1tap30 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       0
*.endl

*.lib tx_1tap31 
*vtx_pre_em_1t4  tx_pre_em_1t[4]   vss       pwrq
*vtx_pre_em_1t3  tx_pre_em_1t[3]   vss       pwrq
*vtx_pre_em_1t2  tx_pre_em_1t[2]   vss       pwrq
*vtx_pre_em_1t1  tx_pre_em_1t[1]   vss       pwrq
*vtx_pre_em_1t0  tx_pre_em_1t[0]   vss       pwrq
*.endl

**** Setting for TX termination
.lib rext
vrterm0 tx_term_sel[0] vss dc pwrq
vrterm1 tx_term_sel[1] vss dc pwrq
vrterm2 tx_term_sel[2] vss dc 0
vtx_vtt[0] tx_vtt[0] vss dc 0
vtx_vtt[1] tx_vtt[1] vss dc 0
.endl

*.lib r85
*vrterm0 tx_term_sel[0] vss dc pwrq
*vrterm1 tx_term_sel[1] vss dc pwrq
*vrterm2 tx_term_sel[2] vss dc pwrq
*vtx_vtt[0] tx_vtt[0] vss dc pwrq
*vtx_vtt[1] tx_vtt[1] vss dc pwrq
*.endl

.lib r100
vrterm0 tx_term_sel[0] vss dc 0
vrterm1 tx_term_sel[1] vss dc pwrq
vrterm2 tx_term_sel[2] vss dc pwrq
vtx_vtt[0] tx_vtt[0] vss dc pwrq
vtx_vtt[1] tx_vtt[1] vss dc pwrq
.endl

*.lib r120
*vrterm0 tx_term_sel[0] vss dc pwrq
*vrterm1 tx_term_sel[1] vss dc 0
*vrterm2 tx_term_sel[2] vss dc pwrq
*vtx_vtt[0] tx_vtt[0] vss dc pwrq
*vtx_vtt[1] tx_vtt[1] vss dc pwrq
*.endl

.lib r150
vrterm0 tx_term_sel[0] vss dc 0
vrterm1 tx_term_sel[1] vss dc 0
vrterm2 tx_term_sel[2] vss dc pwrq
vtx_vtt[0] tx_vtt[0] vss dc pwrq
vtx_vtt[1] tx_vtt[1] vss dc pwrq
.endl

**** Setting for Common Mode
* .lib vcm_pd
* vtx_vtt[0] tx_vtt[0] vss dc 0
* vtx_vtt[1] tx_vtt[1] vss dc 0
* .endl

* .lib vcm_on
* vtx_vtt[0] tx_vtt[0] vss dc pwrq
* vtx_vtt[1] tx_vtt[1] vss dc pwrq
* .endl

**** Setting for VOD  
*.lib vod_2ma
*vvod2 tx_vod_sel[2] vss dc 0
*vvod1 tx_vod_sel[1] vss dc 0
*vvod0 tx_vod_sel[0] vss dc 0
*.endl

.lib vod_4ma
vvod2 tx_vod_sel[2] vss dc 0
vvod1 tx_vod_sel[1] vss dc 0
vvod0 tx_vod_sel[0] vss dc pwrq
.endl

.lib vod_6ma
vvod2 tx_vod_sel[2] vss dc 0
vvod1 tx_vod_sel[1] vss dc pwrq
vvod0 tx_vod_sel[0] vss dc 0
.endl

.lib vod_8ma
vvod2 tx_vod_sel[2] vss dc 0
vvod1 tx_vod_sel[1] vss dc pwrq
vvod0 tx_vod_sel[0] vss dc pwrq
.endl

.lib vod_10ma
vvod2 tx_vod_sel[2] vss dc pwrq
vvod1 tx_vod_sel[1] vss dc 0
vvod0 tx_vod_sel[0] vss dc 0
.endl

.lib vod_12ma
vvod2 tx_vod_sel[2] vss dc pwrq
vvod1 tx_vod_sel[1] vss dc 0
vvod0 tx_vod_sel[0] vss dc pwrq
.endl

*.lib vod_7ma
*vvod2 tx_vod_sel[2] vss dc pwrq
*vvod1 tx_vod_sel[1] vss dc pwrq
*vvod0 tx_vod_sel[0] vss dc 0
*.endl

.lib vod_9ma
vvod2 tx_vod_sel[2] vss dc pwrq
vvod1 tx_vod_sel[1] vss dc pwrq
vvod0 tx_vod_sel[0] vss dc pwrq
.endl

**** slew rate setting
.lib slewoff
vtx_slew1 tx_slew[1]  vss  0
vtx_slew0 tx_slew[0]  vss  0
.endl

.lib slewon_slow
vtx_slew1 tx_slew[1]  vss  0
vtx_slew0 tx_slew[0]  vss  pwrq
.endl

.lib slewon_mid
vtx_slew1 tx_slew[1]  vss  pwrq
vtx_slew0 tx_slew[0]  vss  0
.endl

.lib slewon_fast
vtx_slew1 tx_slew[1]  vss  pwrq
vtx_slew0 tx_slew[0]  vss  pwrq
.endl

