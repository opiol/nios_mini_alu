.subckt OPAMP   1  2  3
.PROT freelib
;W[277-W3/1oto8
53[xH$X#p(/m(a[
'Uc9%2P73+(o2!l
0tT'o$2S7JW39%270yQ
H0[I:MsQ
.UNPROT
.ends OPAMP

.SUBCKT signal_generator input output ref
.PROT freelib
5E$;:1kW250UX1\;:)XhZ=<,:v1-)he5!b2'H\<.y
e$;:1Kw2*6FY]3' %6;j07ws9P;J1#t9Y8dV[
+t'Y=O$%D57)y:ZbR<2*6fk7(W;W0(-d[=F977>08
X256;>7*$<B3;8y#(:71-)HE2OyE1l
!*$;5M2v8Cy9y8o60d$+k22Y7r+Y
e$;:1Kw6kM2Y7jwy
6
6h$4\TrXxZ77R+y
y%28=# C9V:L
s3w+'X3tD#R(e1:7iv'250ux1\;x6
>j$#3w+UX17!]4>*\)-.[k<-=Zv]#Q
7[XZ57\3:=U2W.[929z
3S7\3:=/20x8
5>9[S$#5wP/Z
aS7\h:y2\$]3XM' (=JET'B0/G#dRY
k/28h# p9yjZbup9!uW61ZK[5[ k[
4U:=j28<# p9.iG\*wY
yv28<# W9V:L
85w+$XZ*7R+y
kv28>#59Jx19>]Xp*\y-.[k<-=Zv]8
/)9[t$#]wXUBS*=<6mQ$;/90$+%y0
'j# E9[E$#]w0]=o6[
'j:=f28=JW36
>t$#3+:y2\$]jX((/s-.[k<-=Zv]8
o>9[hP7:#Fx23wrsWTWi'T8T! 9.iG\*wY
6
Z
[$-$[t*62,4S*:\=3h10=99G;h##V25Mi$[
,WZ58z$1D8Rb7OT#sWTM62wrstRxX7>.<;'=>($.'v6WP(/7$ !xo2a1c y
ax\:mKw!=$mj7SV:m4'6RSw!=7D$<jw0FM23m[;Y
az
PWD:k9XxD Dz) 96( 79/U9z
(WRSTrXx/\D:);*pD\%*! 9.\ 9<2:\>P/7:#fX23WRSw;*_2K=,Bp/: :49);wMity[ %*]w0X7q
;M4'6rsW[0_\T8<;007I\%#vRx$MJ7D5!\23H\*)o5[AT[
'Ka0['$m$22072:0:w3Nz
0
Zr\t;M70
.UNPROT
.ends signal_generator
