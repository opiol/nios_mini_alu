.probe tran
+ v(rxippad) v(rxinpad) v(rxippad,rxinpad)
+ v(sg_p) v(sg_n) v(sg_p,sg_n) 
+ v(inpp) v(innn) v(inpp,innn)
+ v(rxip_ball) v(rxin_ball) v(rxip_ball,rxin_ball)
+ v(rxip_bump) v(rxin_bump) v(rxip_bump,rxin_bump)
+ v(rxp) v(rxn) v(rxp,rxn)
+ v(xrx_top.xrx.rxp) v(xrx_top.xrx.rxn)
+ v(rxp_int) v(rxn_int)
