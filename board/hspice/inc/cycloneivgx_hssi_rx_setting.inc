***** Setting for RX VCM Level
.lib vcm_tri
v_rx_vtt2 rx_vtt[2] vss pwrq
v_rx_vtt1 rx_vtt[1] vss pwrq
v_rx_vtt0 rx_vtt[0] vss 0
.endl

.lib vcm_0p825
v_rx_vtt2 rx_vtt[2] vss 0
v_rx_vtt1 rx_vtt[1] vss pwrq
v_rx_vtt0 rx_vtt[0] vss 0
.endl

*.lib vcm_1p1
*v_rx_vtt2 rx_vtt[2] vss 0
*v_rx_vtt1 rx_vtt[1] vss pwrq
*v_rx_vtt0 rx_vtt[0] vss pwrq
*.endl

**** Setting for RX termination
.lib rext
vrx_term2 rx_term[2] vss dc 0
vrx_term1 rx_term[1] vss dc pwrq
vrx_term0 rx_term[0] vss dc pwrq
.endl

*.lib r85
*vrx_term2 rx_term[2] vss dc pwrq
*vrx_term1 rx_term[1] vss dc pwrq
*vrx_term0 rx_term[0] vss dc pwrq
*.endl

.lib r100
vrx_term2 rx_term[2] vss dc pwrq
vrx_term1 rx_term[1] vss dc pwrq
vrx_term0 rx_term[0] vss dc 0
.endl

*.lib r120
*vrx_term2 rx_term[2] vss dc pwrq
*vrx_term1 rx_term[1] vss dc 0
*vrx_term0 rx_term[0] vss dc pwrq
*.endl

.lib r150
vrx_term2 rx_term[2] vss dc pwrq
vrx_term1 rx_term[1] vss dc 0
vrx_term0 rx_term[0] vss dc 0
.endl

**** DC Gain Setting
.lib dc_gain0
.PROT freelib
6%>!g4\3:YeGX4sSoK+)u79K<910
<Ygd'W[j$84S[W7GVS U19-U/7JQ
6%>!g4\p:YeGX4sSoK+hu79K<910
<Ygd'W[:$84S[W7GVS +19-U/7JQ
H0[I:MsQ
.UNPROT
.endl

.lib dc_gain3
.PROT freelib
6%>!g4\3:YeGX4sSoK+)u79K<910
<Ygd'W[j$84S[W7GVS U19-U/7JQ
6%>!g4\p:YeGX4sSoK+hu79K<910
<Ygd'W[:$84S[W7GVS +19-U/7*M;>Z
-\#*50Xz
.UNPROT
.endl

.lib dc_gain6
.PROT freelib
6%>!g4\3:YeGX4sSoK+)u79K<910
<Ygd'W[j$84S[W7GVS U19-U/7JQ
6%>!g4\p:YeGX4sSoK+hu79K<9$4:A8
yOkG;h#jw;3d/h%/;%11%$+zk:]C3_[
+!+]6dJ[
.UNPROT
.endl

*.lib dc_gain9
*.prot
*vdcgain3 rx_bit_dc[3] vss 0
*vdcgain2 rx_bit_dc[2] vss pwrq
*vdcgain1 rx_bit_dc[1] vss pwrq
*vdcgain0 rx_bit_dc[0] vss pwrq
*.unprot
*.endl

*.lib dc_gain12
*.prot
*vdcgain3 rx_bit_dc[3] vss pwrq
*vdcgain2 rx_bit_dc[2] vss pwrq
*vdcgain1 rx_bit_dc[1] vss pwrq
*vdcgain0 rx_bit_dc[0] vss pwrq
*.unprot
*.endl

************** Equalizer Settings *************
.lib eq_1p5
.PROT freelib
6*N;.SsW9>=G[%'=:\0U$YgW(6
-(_-// 92nX6g2;rXp/9#;%910
<0_/qK72TAgS%x6l$+ZK:%>2P[
9)4%DAJ7<%oC>':,#Y<bX [7JQ
6*Np.SsW9>-G[%'=:\0U$YgW2d6nL
!$ 2y.'l
.UNPROT
.endl

.lib eq_5p5
.PROT freelib
6*N;.SsW9>=G[%'=:\0U$YgW2d6nL
+<%[c>'$0_FQk73379k<9pa$X8
y9>>g[%#)4kdaj8%w6B02vs#+Y
\2Nv6G2:*n .sS5f9-u/7d/:/z
Pta9s%Xx(_+// yI2<9zwOkx:l
!$ 2y.'l
.UNPROT
.endl

.lib eq_4p5
.PROT freelib
6*N;.SsW9>=G[%'=:\0U$YgW2d6nL
+<%[c>'$0_FQk73379k<9pa$X8
y9>>g[%#)4kdaj8%w6B02vs#+Y
\2Nv6G2:*n .sS5f9-u/7d/:/z
Pta9s%Xx(_+// yI2<9zwOkxi,8_q
H0[I:MsQ
.UNPROT
.endl

.lib eq_7
.PROT freelib
6*N;.SsW9>=G[%'=:\0U$YgW2d6nL
+<%[c>'$0_FQk73379k<9pa$.x546
-(_S// 92n[6g2;rXp/9#;%910
<0_;qK72TADS%x6l$+ZK:%>2P[
9)4\DAJ7<%6C>':,#Y<bX [7*M;>Z
-\#*50Xz
.UNPROT
.endl

