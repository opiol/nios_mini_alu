###############################################################################
# Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
# Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
# support information,  device programming or simulation file,  and any other
# associated  documentation or information  provided by  Altera  or a partner
# under  Altera's   Megafunction   Partnership   Program  may  be  used  only
# to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
# other  use  of such  megafunction  design,  netlist,  support  information,
# device programming or simulation file,  or any other  related documentation
# or information  is prohibited  for  any  other purpose,  including, but not
# limited to  modification,  reverse engineering,  de-compiling, or use  with
# any other  silicon devices,  unless such use is  explicitly  licensed under
# a separate agreement with  Altera  or a megafunction partner.  Title to the
# intellectual property,  including patents,  copyrights,  trademarks,  trade
# secrets,  or maskworks,  embodied in any such megafunction design, netlist,
# support  information,  device programming or simulation file,  or any other
# related documentation or information provided by  Altera  or a megafunction
# partner, remains with Altera, the megafunction partner, or their respective
# licensors. No other licenses, including any licenses needed under any third
# party's intellectual property, are provided herein.
#
###############################################################################


# FPGA Xchange file generated using Quartus Prime Version 15.1.2 Build 193 02/01/2016 SJ Lite Edition

# DESIGN=nios
# REVISION=nios
# DEVICE=EP4CGX15
# PACKAGE=FBGA
# SPEEDGRADE=6

Signal Name,Pin Number,Direction,IO Standard,Drive (mA),Termination,Slew Rate,Swap Group,Diff Type

led[0],A7,output,1.8 V,Default,Series 50 Ohm without Calibration,FAST,swap_0,--
led[1],A6,output,1.8 V,Default,Series 50 Ohm without Calibration,FAST,swap_0,--
clk25b,A10,input,1.8 V,,Off,--,swap_1,--
altera_reserved_tms,A2,input,2.5 V,,Off,--,NOSWAP,--
altera_reserved_tck,B3,input,2.5 V,,Off,--,NOSWAP,--
altera_reserved_tdi,A3,input,2.5 V,,Off,--,NOSWAP,--
altera_reserved_tdo,A1,output,2.5 V,Default,Off,FAST,NOSWAP,--
auto_stp_external_clock_0,J7,input,2.5 V,,Off,--,swap_2,--
~ALTERA_NCEO~,N5,output,2.5 V,16,Off,FAST,NOSWAP,--
~ALTERA_DATA0~,A5,input,2.5 V,,Off,--,NOSWAP,--
~ALTERA_ASDO~,B5,input,2.5 V,,Off,--,NOSWAP,--
~ALTERA_NCSO~,C5,input,2.5 V,,Off,--,NOSWAP,--
~ALTERA_DCLK~,A4,output,2.5 V,Default,Off,FAST,NOSWAP,--
