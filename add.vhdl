-- adder module

--A B Ci		S Co
--0 0 0		0 0
--0 0 1		1 0
--0 1 0		1 0
--1 0 0		1 0
--1 0 1		0 1
--1 1 0 		0 1
--1 1 1		1 1

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;						-- needed for +/- operations


entity add_2bit is
	Port(
		a	:	in  std_logic;								
		b	:	in  std_logic;
		ci :	in  std_logic;
		
		q	: 	out std_logic;
		co : 	out std_logic
	);
end add_2bit;

architecture Behavioral of add_2bit is
begin
--	q <= ( not(a) and not(b) and ci) or (not(a) and b and ci) or (a and b and ci) or (a and not(b) and ci) ;
--	co <= ( a and b) or ( a and ci) or (b and ci);

q <= (a xor b) xor ci;
co <= (a and b) or (ci and ( a xor b)); 
end Behavioral;

---------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;						

entity add_32bit is
port (
	a : in std_logic_vector(31 downto 0);
	b : in std_logic_vector(31 downto 0);
	ci :	in  std_logic;
	
	Q : out std_logic_vector(31 downto 0);
	co : out std_logic
);
end add_32bit;

architecture BEH of add_32bit is
component add_2bit
  port(		
	a : in std_logic;
	b : in std_logic;
	ci : in std_logic;
	
	q : out std_logic;
	co : out std_logic
);
end component add_2bit;
signal carry_sig: std_logic_vector(32 DowNto 0);
begin
 
-- A1: add_2bit port map (a(0), b(0), ci, q(0), carry_sig(0));
--  A2: add_2bit port map (a(1), b(1), carry_sig(0), q(1), carry_sig(1));
--  A3: add_2bit port map (a(2), b(2), carry_sig(1), q(2), carry_sig(2));
--  A4: add_2bit port map (a(3), b(3), carry_sig(2), q(3), co);
 	carry_sig(0) <= ci;
	co <= carry_sig(32) ;
	aa :for i in 0 to 31 generate
		AR_ADD : add_2bit port map (a(i), b(i), carry_sig(i), q(i), carry_sig(i+1));	
	end generate aa; 
  
  end BEH;	

