

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;						-- needed for +/- operations


entity sub_2bit is
	Port(
		a	:	in  std_logic;								
		b	:	in  std_logic;
		bi :	in  std_logic;
		
		q	: 	out std_logic;
		bo : 	out std_logic
	);
end sub_2bit;

architecture Behavioral of sub_2bit is
begin


q <= (a xor b) xor bi ;
bo <= ( not(a) and b ) or ( not(a xor b) and bi); 
end Behavioral;

---------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;						

entity sub_32bit is
port (
	a : in std_logic_vector(31 downto 0);
	b : in std_logic_vector(31 downto 0);
	bi :	in  std_logic;
	
	Q : out std_logic_vector(31 downto 0);
	bo : out std_logic
);
end sub_32bit;

architecture BEH of sub_32bit is
component sub_2bit
  port(		
	a : in std_logic;
	b : in std_logic;
	bi : in std_logic;
	
	q : out std_logic;
	bo : out std_logic
);
end component sub_2bit;
signal carry_sig: std_logic_vector(32 DowNto 0);
begin
 
 	carry_sig(0) <= bi;
	bo <= carry_sig(32) ;
	aa :for i in 0 to 31 generate
		AR_sub : sub_2bit port map (a(i), b(i), carry_sig(i), q(i), carry_sig(i+1));	
	end generate aa; 
  
  end BEH;	

