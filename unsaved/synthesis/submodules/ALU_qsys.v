// alu qsys integration



module ALU_qsys (
	input clk,
	input resetn,
	input reset,
	input write,
	input read,
	input [31:0] writedata,
	output reg [31:0] readdata,
	input [5:0] address
	);

//	regmap:
//10: status
//	c: result
//	8:	operand
//	4: value B
//	0: value A
	reg [31:0] status, result, op, valA, valB;
	reg [31:0] and_result,or_result,add_result;
	
	log_and_32bit myand(.a(valA), .b(valB), .q(and_result));
	log_or_32bit myor(.a(valA), .b(valB), .q(or_result));
	add_32bit 	myadd(.a(valA), .b(valB), .q(add_result));
	
	always @(posedge clk) begin
		if( write == 1) begin
			case(address)
				32'h0: valA = writedata;
				32'h4: valB = writedata;
				32'h8: 
					begin
					op = writedata;
					case(op)
						32'h1:result = and_result;
						32'h2:result = or_result;
						32'h3:result = add_result;
					endcase	
					end
			endcase
		end	
		else if (read == 1) begin
			case(address)
				32'h0: readdata <= valA ;
				32'h4: readdata <= valB;
				32'h8: readdata <= op;
				32'hc: readdata = result;
				32'h10: readdata <= status;
			endcase
		end
	end	
endmodule