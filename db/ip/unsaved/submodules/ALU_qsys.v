// alu qsys integration



module ALU_qsys (
	input clk,
	input resetn,
	input reset,
	input write,
	input read,
	input [31:0] writedata,
	output reg [31:0] readdata,
	input [5:0] address
	);

//	regmap:
//10: status
//	c: result
//	8:	operand
//	4: value B
//	0: value A
	reg [31:0] status, result, op, valA, valB;
	reg [31:0] and_result, or_result, add_result, sub_result, not_result;
	reg [31:0] add_status, sub_status;
	
	log_and_32bit	myand(.a(valA), .b(valB), .q(and_result));
	log_or_32bit 	myor(.a(valA), .b(valB), .q(or_result));
	add_32bit 		myadd(.a(valA), .b(valB), .ci(0), .q(add_result), .co(add_status[0]));
	sub_32bit		mysub(.a(valA), .b(valB), .bi(0), .q(sub_result), .bo(sub_status[1]));
	log_not_32bit	mynot(.a(valA), .q(not_result));
	
	always @(posedge clk) begin
		if( write == 1) begin
			case(address)
				32'h0: valA = writedata;
				32'h4: valB = writedata;
				32'h8: 
					begin
					op = writedata;
					case(op)
						32'h1: begin result = and_result; status = 0 ; end
						32'h2: begin result = or_result;  status = 0 ; end
						32'h3: begin result = add_result; status = add_status; end
						32'h4: begin result = sub_result; status = sub_status; end
						32'h5: begin result = not_result; status = 0; end
					endcase	
					end
			endcase
		end	
		else if (read == 1) begin
			case(address)
				32'h0: readdata 	= valA ;
				32'h4: readdata 	= valB;
				32'h8: readdata 	= op;
				32'hc: readdata 	= result;
				32'h10: readdata 	= status;
			endcase
		end
	end	
endmodule