module top(
	input clk25b,
	output [1:0] led
	);

wire clk100M;
	
	mpll2 pll(
		.inclk0(clk25b),
		.c0(clk100M),
	);
	
    unsaved u0 (
        .clk_clk       (clk100M),       //   clk.clk
        .reset_reset_n (1)
    );
	
endmodule