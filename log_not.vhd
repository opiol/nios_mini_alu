
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;						-- needed fnot +/- operations


entity log_not_1bit is
	Port(
		a	:	in  std_logic;															
		q	: 	out std_logic
	);
end log_not_1bit;

architecture Behavioral of log_not_1bit is
begin
			q <= not(a);
end Behavioral;

---------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;						-- needed for +/- operations

entity log_not_32bit is
port (
	a : in std_logic_vector(31 downto 0);
	Q : out std_logic_vector(31 downto 0)
);
end log_not_32bit;

architecture BEH of log_not_32bit is
component log_not_1bit
  port(		
	a : in std_logic;
	Q : out std_logic
);
end component log_not_1bit;
begin
	aa :for i in 0 to 31 generate
		begin
		LOG_NOT : log_not_1bit
		port map (
			a(i),
			q(i)
		);
	end generate aa;
	
end BEH;	

